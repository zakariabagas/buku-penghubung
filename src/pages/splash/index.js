import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { Component, useEffect } from 'react';
import { View, Text, StatusBar, Image } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { useDispatch } from 'react-redux';
import { setApiAction } from '../../redux';


const Splash = ({navigation}) => {
    useEffect(() => {
        // const cekAsync = await AsyncStorage.getItem('user')
        // const cekAsync1 = await AsyncStorage.getItem('pass')
        // setTimeout(() => {
        //     console.warn(cekAsync)
        //     if(cekAsync === "160631100071" ) {
        //         navigation.replace('HomeGuru')
        //         console.log(cekAsync)
        //     } else {
        //         navigation.replace('Login')
        //     }
        // }, 2000)
        cekAsync()
    })

    // const dispatch = useDispatch();
    const cekAsync = async () => {
        // try {
        // } catch(e) {
        //     console.warn(e)
        // }
        const isLogin = await AsyncStorage.getItem('status')
        setTimeout(() => {
            console.warn(isLogin)
            if(isLogin === "guru" ) {
                // dispatch({type:'SET_USER_PASS'})
                // dispatch(setApiAction())
                // console.log('ini apa hayo? ',dispatch(setApiAction()))
                navigation.replace('HomeGuru')
                console.log(isLogin)
            } else {
                navigation.replace('Login')
            }
        }, 2000)
    }

    return(
        <LinearGradient colors={['#4E7934','#1A4709']} style={{ flex:1, alignItems:'center', justifyContent:'center' }}>
        <StatusBar translucent backgroundColor="transparent" />
            <View style={{alignItems:'center'}}>
                <Image source={require('../../assets/images/logo-sdit.png')} style={{width:80, height:80, marginBottom: 20}}/>      
                <Text style={{ fontFamily:'ProductSans-Black',fontSize:15, color:'white', textAlign:'center'}}>BUKU PENGHUBUNG</Text>
                <Text style={{ fontFamily:'ProductSans-Light',fontSize:12, color:'white', textAlign:'center'}}>SDIT Ulil Albab Kamal</Text>
            </View>      
            <View style={{position:"absolute", bottom:20}}>
            <Text style={{ fontFamily:'ProductSans-Light',fontSize:12, color:'white', textAlign:'center', opacity:0.5}}>Versi 1.0.0</Text>
            </View>
        </LinearGradient>
    )
}
export default Splash;