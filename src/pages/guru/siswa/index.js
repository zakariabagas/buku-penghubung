import * as React from 'react';
import { Text, View, Image } from 'react-native';
import Header from '../../../organisms/header';
import { Searchbar } from 'react-native-paper';
import { onChange } from 'react-native-reanimated';
import { IconSvg } from '../../../atoms/svg/admin';
import Icon from 'react-native-vector-icons/FontAwesome';
import { TouchableOpacity } from 'react-native-gesture-handler';

const index = () => {
    const myIcon = <Icon name="search" size={20} color="#f0f0f0" />;
    const [searchQuery, setSearchQuery] = React.useState('');
    const onChangeSearch = query => setSearchQuery(query);
    return (
        <View style={{flex: 1, backgroundColor: '#f8f8f8'}}>
            <Header title="Lihat Data Siswa"/>
            <View style={{flex:1}}>
                <View style={{marginHorizontal:25, marginTop:20}}>
                    <Searchbar 
                        inputStyle ={{fontSize:15, color:'#f0f0f0', fontFamily:'ProductSans-Bold'}}
                        style={{backgroundColor:'#4E7934', borderRadius:100/2, paddingHorizontal:5, color:'#fofofo', marginBottom:10}}
                        icon={() => myIcon}
                        placeholder="Cari Nama"
                        onChangeText={onChangeSearch}
                        value={searchQuery}
                    />
                    <TouchableOpacity>
                        <View style={{backgroundColor:'white', borderRadius:50, marginVertical:5, alignItems:'center', flexDirection:'row', paddingVertical:12, paddingHorizontal:15, shadowColor: "#000",
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 1,
                            },
                            shadowOpacity: 0.22,
                            shadowRadius: 2.22,

                            elevation: 3,
                        }}>
                            <Text style={{fontFamily:'ProductSans-Bold', fontSize:25}}>1</Text>
                            <Image source={require('../../../assets/images/foto.jpg')} style={{width:45, height:45, borderRadius:45/2, marginHorizontal:10}}></Image>
                            <View style={{}}>
                                <View style={{flexDirection:'row', width:190}}>
                                    <Text numberOfLines={1} ellipsizeMode='tail' style={{color:'#1A4709', fontFamily:'ProductSans-Medium', fontSize:14, flexShrink:1 }}>Muh. Zakaria Bagas Rizqi Firdaus as as asas  </Text>
                                </View>
                                <Text style={{color:'#4E7934', fontFamily:'ProductSans-Regular', fontSize:12,}}>160631100071</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={{backgroundColor:'white', borderRadius:50, marginVertical:5, alignItems:'center', flexDirection:'row', paddingVertical:12, paddingHorizontal:15, shadowColor: "#000",
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 1,
                            },
                            shadowOpacity: 0.22,
                            shadowRadius: 2.22,

                            elevation: 3,
                        }}>
                            <Text style={{fontFamily:'ProductSans-Bold', fontSize:25}}>1</Text>
                            <Image source={require('../../../assets/images/foto.jpg')} style={{width:45, height:45, borderRadius:45/2, marginHorizontal:10}}></Image>
                            <View style={{}}>
                                <View style={{flexDirection:'row', width:190}}>
                                    <Text numberOfLines={1} ellipsizeMode='tail' style={{color:'#1A4709', fontFamily:'ProductSans-Medium', fontSize:14, flexShrink:1 }}>Muh. Zakaria Bagas Rizqi Firdaus as as asas  </Text>
                                </View>
                                <Text style={{color:'#4E7934', fontFamily:'ProductSans-Regular', fontSize:12,}}>160631100071</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={{backgroundColor:'white', borderRadius:50, marginVertical:5, alignItems:'center', flexDirection:'row', paddingVertical:12, paddingHorizontal:15, shadowColor: "#000",
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 1,
                            },
                            shadowOpacity: 0.22,
                            shadowRadius: 2.22,

                            elevation: 3,
                        }}>
                            <Text style={{fontFamily:'ProductSans-Bold', fontSize:25}}>20</Text>
                            <Image source={require('../../../assets/images/foto.jpg')} style={{width:45, height:45, borderRadius:45/2, marginHorizontal:10}}></Image>
                            <View style={{}}>
                                <View style={{flexDirection:'row', width:190}}>
                                    <Text numberOfLines={1} ellipsizeMode='tail' style={{color:'#1A4709', fontFamily:'ProductSans-Medium', fontSize:14, flexShrink:1 }}>Muh. Zakaria Bagas Rizqi Firdaus as as asas  </Text>
                                </View>
                                <Text style={{color:'#4E7934', fontFamily:'ProductSans-Regular', fontSize:12,}}>160631100071</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    
                </View>
            </View>
        </View>
    )
}

export default index
