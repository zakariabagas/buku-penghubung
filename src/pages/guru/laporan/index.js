import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Text, View, Image } from 'react-native';
import Header from '../../../organisms/header'

const index = () => {
    const dataForAPI = {
        name:'taek',
        job:'ngentu'
    }
    const [dataAPI, setDataAPI] = useState({
        first_name:'',
        last_name:'',
        email:'',
        avatar:''
    })
    useEffect(() => {
        // fetch('https://reqres.in/api/users?page=2')
        // .then(response => response.json())
        // .then(json => console.log(json))
        // console.log('Data Object', dataForAPI)
        // console.log('Data Stringify', JSON.stringify(dataForAPI))
        
        //AXIOS UNTUK GET

        /* Axios.get('https://reqres.in/api/unknown/2')
        .then(function(response) {
            console.log(response);
        })
        .catch(function(error){
            console.log(error);
        }) */

        //AXIOS UNTUK POST

        /* Axios.post('https://reqres.in/api/register', {
            "email": "eve.holt@reqres.in",
            "password": "pistol"
        })
        .then(function(response) {
            console.log(response);
        })
        .catch(function(error) {
            console.log(error);
        }) */
    }, [])

    const getData = () => {
        Axios.get('url')
        .then(res => (
            console.log('res : ', res)
        ))
    }

    return (
        <View style={{flex: 1, backgroundColor: '#f8f8f8'}}>
            <Header title="Lihat Laporan"/>
            <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
            <View style={{alignItems:'center'}}>
                    <Image source={require('../../../assets/images/foto.jpg')} style={{width:145, height:145, borderRadius:145/2, marginVertical:20}}></Image>
                    <Text style={{fontFamily:'ProductSans-Black', fontSize:20, color:'#1A4709'}}>Pak Bagas</Text>
                    <Text style={{fontFamily:'ProductSans-Medium', fontSize:16, color:'#4E7934'}}>160631100071</Text>
                    <Text style={{fontFamily:'ProductSans-Medium', fontSize:16, color:'#4E7934'}}>2A</Text>
                </View>
                <View style={{marginVertical:20}}>
                    <Text style={{fontFamily:'ProductSans-Reguler', fontSize:14, textAlign:'center'}}>Muh. Zakaria Bagas Rizqi Firdaus</Text>
                    <Text style={{fontFamily:'ProductSans-Reguler', fontSize:14, textAlign:'center'}}>Laki-laki</Text>
                    <Text style={{fontFamily:'ProductSans-Reguler', fontSize:14, textAlign:'center'}}>Surabaya,03 September 1998</Text>
                    <Text style={{fontFamily:'ProductSans-Reguler', fontSize:14, textAlign:'center'}}>Jl. Gading Karya 5 no. 70, Surabaya</Text>
                    <Text style={{fontFamily:'ProductSans-Reguler', fontSize:14, textAlign:'center'}}>0838-3034-3368</Text>
                </View>
            </View>
        </View>
    )
}

export default index