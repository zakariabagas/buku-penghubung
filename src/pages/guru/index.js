import React, { Component, useEffect, useState } from "react";
import { 
    View,
    Text,
    StyleSheet,
    Image,
    StatusBar,
    TouchableOpacity
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import Icon from "../../atoms/svg";
import NavIcon from "../../molecules/navicon";
import NavBar from "../../organisms/navbar";
import MenuIcon from '../../molecules/menuicon';
import MenuHome from "../../organisms/menuhome";
import Axios from "axios";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useDispatch, useSelector } from "react-redux";
import { setApi } from "../../redux";
// import {setApi}

const HomeGuru = ({navigation}) => {
    
    const [users,setUsers] = useState([]);
    const LoginReducer = useSelector(state => state.loginDataGuruSetReducer);
    const dispatch = useDispatch();
    useEffect(() => {
        getAsyncStorage();
        console.log('reducer ', LoginReducer)
        // console.log(getData)
        // console.warn(getAsyncStorage());
    }, []);

    const getAsyncStorage = async () => {
        try {
            const user = await AsyncStorage.getItem('user')
            const pass = await AsyncStorage.getItem('pass')
            getData(user,pass)
            // console.log('log dari getData : ',getData(user,pass))
        } catch(e) {

        } 
    }

    const getData = (user, pass) => {
        Axios.get(`http://192.168.43.65/api/?username=${user}&password=${pass}`)
            .then(function (response) {
                console.log(response.data[0]);
                setUsers(response.data);
                // console.log(response.data)
                // dispatch({type: 'SET_API', (response.data[0])})
                // dispatch(setApi(response.data));
                // dispatch({type: "SET_API",
                // payload: response.data})
                // response.json();
            })
            .catch(function(error) {
                console.log(error);
            })
            .then(function() {

            });
    };

    const BioPengguna = (props) => {
        return (
            <View style={styles.bio}>
                <View style={{alignItems:'center'}}>
                    <View>
                        {/* <Image source={require('../../assets/images/foto.jpg')} style={{width:140, height:140, borderRadius:140/2, marginBottom:20}}></Image> */}
                        <Image source={{uri: props.ava}} style={{width:140, height:140, borderRadius:140/2, marginBottom:20}}></Image>
                    </View>
                    <View style={{marginBottom:40}}>
                        <Text style={styles.titleHead}>{props.nama}</Text>
                        <View style={{flexDirection:'row', alignSelf:'center'}}>
                            <Text style={styles.subtitleHead}>{props.status}</Text>
                            <Text style={styles.subtitleHead}>{props.kelas}</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <StatusBar translucent backgroundColor="transparent" />
            {/* Header */}
            <View style={styles.header}>
                <View style={styles.head}>
                    <View style={{justifyContent:'center'}}>
                        <Image source={require('../../assets/images/logo-sdit.png')} style={{width:25, height:25}}/>      
                    </View>
                    <View style={{marginLeft:10}}>
                        <Text style={{ fontFamily:'ProductSans-Black',fontSize:10, color:'white', textAlign:'left'}}>BUKU PENGHUBUNG</Text>
                        <Text style={{ fontFamily:'ProductSans-Light',fontSize:10, color:'white', textAlign:'left'}}>SDIT Ulil Albab Kamal</Text>
                    </View>
                </View>
            </View>
            {/* Badan Page */}
            <ScrollView style={{flex:1}}>
                {/* Bio Pengguna */}
                {users.map(user => {
                    const jk = user.g_jk == '1' ? 'Pak ':'Bu ';
                    var image = `data:image/png;base64,${user.g_foto}`;
                    return <BioPengguna ava={image} nama={jk+user.g_nm_panggilan} status={'Guru Kelas'} kelas={'2A'} />
                })}
                {/* <BioPengguna nama={'Pak Bagas'} status={'Guru Kelas'} kelas={'2A'} /> */}
                {/* Menu */}
                {/* <View style={{flexDirection: 'row', flexWrap:'wrap', marginHorizontal:30, marginVertical:20}}>
                    <View style={{justifyContent: 'space-between', flexDirection:'row', width: '100%', flexWrap:"wrap"}}>
                        <View style={{width: '50%', height:150, justifyContent:'center', alignItems:'center'}}>
                            <TouchableOpacity style={{alignItems:"center", justifyContent:'center'}} onPress={() => navigation.navigate('Isi Evaluasi')}>
                                <Icon name="isi buku penghubung" height={65} fill={'#1A4709'} />
                                <Text style={{fontFamily:'ProductSans-Medium', fontSize:16, color:'#1A4709', textAlign:'center', marginTop:10}}>Isi Buku Penghubung</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{width: '50%', height:150, justifyContent:'center', alignItems:'center'}}>
                            <TouchableOpacity style={{alignItems:"center", justifyContent:'center'}}>
                                <Icon name="catatan khusus" height={65} fill={'#1A4709'} />
                                <Text style={{fontFamily:'ProductSans-Medium', fontSize:16, color:'#1A4709', textAlign:'center', marginTop:10}}>Catatan Khusus</Text>
                            </TouchableOpacity>
                        </View>
                        <MenuIcon nama="Catatan" iconName="catatan khusus" screenName={'Catatan Khusus'}/>
                        <View style={{width: '50%', height:150, justifyContent:'center', alignItems:'center'}}>
                            <TouchableOpacity style={{alignItems:"center", justifyContent:'center'}}>
                                <Icon name="siswa" height={65} fill={'#1A4709'} />
                                <Text style={{fontFamily:'ProductSans-Medium', fontSize:16, color:'#1A4709', textAlign:'center', marginTop:10}}>Lihat Data Siswa</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{width: '50%', height:150, justifyContent:'center', alignItems:'center'}}>
                            <TouchableOpacity style={{alignItems:"center", justifyContent:'center'}}>
                                <Icon name="guru" height={65} fill={'#1A4709'} />
                                <Text style={{fontFamily:'ProductSans-Medium', fontSize:16, color:'#1A4709', textAlign:'center', marginTop:10}}>Cek Profil Guru</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{width: '50%', height:150, justifyContent:'center', alignItems:'center'}}>
                            <TouchableOpacity style={{alignItems:"center", justifyContent:'center'}}>
                                <Icon name="laporan" height={65} fill={'#1A4709'} />
                                <Text style={{fontFamily:'ProductSans-Medium', fontSize:16, color:'#1A4709', textAlign:'center', marginTop:10}}>Lihat Laporan</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{width: '50%', height:150, justifyContent:'center', alignItems:'center'}}>
                            <TouchableOpacity style={{alignItems:"center", justifyContent:'center'}}>
                                <Icon name="keluar" height={65} fill={'#1A4709'} />
                                <Text style={{fontFamily:'ProductSans-Medium', fontSize:16, color:'#1A4709', textAlign:'center', marginTop:10}}>Keluar</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View> */}
                <MenuHome user="guru" />
                <TouchableOpacity onPress={()=>{console.log('reducer ', LoginReducer);}}><Text>show data</Text></TouchableOpacity>
            </ScrollView>
            {/* NavBar */}
            {/* <View style={{height:80,flexDirection:'row',backgroundColor:'#1A4709'}}>
                <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                    <TouchableOpacity style={{alignItems:"center"}}>
                        <Icon name="beranda" height={30} />
                        <Text style={styles.teksMenu}>Beranda</Text>
                    </TouchableOpacity>
                </View>
                <NavIcon namaIcon={"beranda"} teks={"Beranda"} />
                <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                    <TouchableOpacity style={{alignItems:"center"}}>
                        <Icon name="isi buku penghubung" height={30} />
                        <Text style={styles.teksMenu}>Isi Evaluasi</Text>
                    </TouchableOpacity>
                </View>
                <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                    <TouchableOpacity style={{alignItems:"center"}}>
                        <Icon name="catatan khusus" height={30} />
                        <Text style={styles.teksMenu}>Catatan Khusus</Text>
                    </TouchableOpacity>
                </View>
                <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                    <TouchableOpacity style={{alignItems:"center"}}>
                        <Icon name="siswa" height={30} />
                        <Text style={styles.teksMenu}>Data Siswa</Text>
                    </TouchableOpacity>
                </View>
                <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                    <TouchableOpacity style={{alignItems:"center"}}>
                        <Icon name="guru" height={30} />
                        <Text style={styles.teksMenu}>Pengguna</Text>
                    </TouchableOpacity>
                </View>
            </View> */}
            {/* <NavBar user="guru" /> */}
        </View>
    );
}
export default HomeGuru;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f8f8f8'
    },
    header: {
        paddingTop:25,
        backgroundColor: '#1A4709'        
    },
    head: {
        marginLeft:20,
        paddingVertical:15,
        flexDirection:'row'
    },
    bio:{
        backgroundColor: '#4E7934',
        borderBottomLeftRadius:80,
        borderBottomRightRadius:80,
        paddingVertical:40,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,

        elevation: 10,
    },
    titleHead: {
        fontFamily:'ProductSans-Black',
        fontSize:24,
        color:'white',
        textAlign:'center',
    },
    subtitleHead: {
        fontFamily:'ProductSans-Bold',
        fontSize:17,
        color:'white',
        marginHorizontal:5,
    },
    teksMenu: {
        fontFamily:'ProductSans-Reguler',
        fontSize:10,
        color:'white',
        textAlign:'center',
        marginTop:5,
    }
});