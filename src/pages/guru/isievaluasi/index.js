import React, { Component } from "react";
import { 
    View,
    Text,
    StyleSheet,
    Image,
    StatusBar, 
    TextInput,
    Dimensions
} from "react-native";
import { ScrollView, TouchableOpacity, TouchableWithoutFeedback } from "react-native-gesture-handler";
import Header from "../../../organisms/header";
import Icon from "../../../atoms/svg";
import NavIcon from "../../../molecules/navicon";
import NavBar from "../../../organisms/navbar";
import Textinput from "../../../atoms/textinput";
import { Dropdown } from 'react-native-material-dropdown-v2';
/* import SegmentedControl from '../../../atoms/segmentedcontrol'; */
import SegmentedControl from "rn-segmented-control";


const index = ({navigation}) => {
    const data =[{
        value: 'Rizka Diana Vita',
    }, {
        value: 'Muh. Zakaria Bagas',
    }, {
        value: 'Lilik Fadilah',
    }];
    const getCurrentDate=()=>{
        var date = new Date().getDate();
        var month = new Date().getMonth() + 1;
        var year = new Date().getFullYear();
  
        //Alert.alert(date + '-' + month + '-' + year);
        // You can turn it in to your desired format
        return date + '-' + month + '-' + year;//format: dd-mm-yyyy;
    }

    const [tabIndex, setTabIndex] = React.useState(1);
    const [theme, setTheme] = React.useState("LIGHT");
    const toggleTheme = () =>
        theme === "LIGHT" ? setTheme("DARK") : setTheme("LIGHT");
    const handleTabsChange = (index) => {
        setTabIndex(index);
    };

    return (
        <View style={styles.container}>
            <Header title="Isi Data Buku Penghubung"/>
            <ScrollView>
                <View style={{flex:1}}>
                    <View style={{marginHorizontal:25, marginTop:20}}>
                        <View style={{marginBottom:10}}>
                            <Text style={{fontFamily:'ProductSans-Medium',color:'#4E7934', }}>Tahun Ajaran</Text>
                            <Textinput editable={false} placeHolder={"2020/2021"}/>
                        </View>
                        <View style={{marginBottom:10}}>
                            <Text style={{fontFamily:'ProductSans-Medium',color:'#4E7934', }}>Tanggal</Text>
                            <Textinput editable={false} placeHolder={"02-11-2020"}/>
                        </View>
                        <View style={{marginBottom:10}}>
                            <Text style={{fontFamily:'ProductSans-Medium',color:'#4E7934', }}>Kelas</Text>
                            <Textinput editable={false} placeHolder={"2A"}/>
                        </View>
                        <View>
                            <Text style={{fontFamily:'ProductSans-Medium',color:'#4E7934', }}>Nama Siswa</Text>
                            <Dropdown label='Pilih Nama Siswa' data={data} />
                        </View>
                        <View style={{marginBottom:10}}>
                            <Text style={{fontFamily:'ProductSans-Medium',color:'#4E7934', }}>Indikator 1</Text>
                            <SegmentedControl
                                tabs={["Baik", "Cukup", "Kurang"]}
                                onChange={() => {}}
                                segmentedControlBackgroundColor="#C5C5C5"
                                activeSegmentBackgroundColor="#1A4709"
                                activeTextColor="white"
                                textColor="black"
                                paddingVertical={10}
                                textStyle={{
                                    fontFamily:'ProductSans-Medium',
                                    fontSize:11
                                }}
                                containerStyle={{
                                    marginVertical: 5,
                                }}
                                width={Dimensions.get("screen").width-50}
                                currentIndex={tabIndex}
                                onChange={handleTabsChange}
                                theme={theme}
                            />
                        </View>
                        
                        <View style={{marginBottom:10}}>
                            <TouchableOpacity style={styles.tombol} onPress={() => {alert('Data Telah Tersimpan');}}>
                                <View>
                                    <Text style={styles.tombolInside}>Simpan</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    );
}

export default index;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f8f8f8'
    },
    header: {
        paddingTop:25,
        backgroundColor: '#1A4709'        
    },
    head: {
        marginLeft:20,
        paddingVertical:15,
        flexDirection:'row',
        justifyContent:'center'
    },
    fonetikBenar: {
        fontFamily:'ProductSans-Medium',
        fontSize:11,
        borderColor:'#1A4709',
        color:'#4E7934',
        borderBottomWidth:1,
        paddingVertical:5,
        paddingHorizontal:10,
        marginTop:5
    },
    fonetikSalah: {
        fontFamily:'ProductSans-Medium',
        fontSize:11,
        borderColor:'#707070',
        borderBottomWidth:1,
        backgroundColor:'#C5C5C5',
        paddingVertical:5,
        paddingHorizontal:10,
        marginTop:5
    },
    tombol: {
        marginTop:10,
        backgroundColor:'#1A4709',
        alignItems:'center',
        padding:10,
        borderRadius:16
    },
    tombolInside: {
        fontFamily:'ProductSans-Medium',
        fontSize:16,
        color:'white'
    }
});
