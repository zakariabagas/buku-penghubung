import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Header from '../../../organisms/header'

const index = ({ navigation }) => {
    const [users, setUsersProfile] = useState([]);
    const [jk, setJk] = useState("");

    useEffect(() => {
        getAsyncStorage();
        // console.log(getData(160631100071,zagasrazor))
        console.log('zagasraszor')
    }, []);

    const getAsyncStorage = async () => {
        try {
            const username = await AsyncStorage.getItem('user');
            const password = await AsyncStorage.getItem('pass');
            console.log('Username : '+username+' & Password : '+password);
            getData(username, password);
        } catch(e) {
            console.log(e);
        }
    }

    const getData = (user, pass) => {
        Axios.get(`http://192.168.43.65/api/?username=${user}&password=${pass}`)
            .then(function (response) {
                // console.log(response.data);
                setUsers(response.data);
                setJk(response.data[0].g_jk==0?"Bu":"Pak");
                console.log(response.data);
            })
            .catch(function(error) {
                console.log(error);
            });
    };
    // const BioPengguna = (props) => {
    //     return (
    //         <View style={styles.bio}>
    //             <View style={{alignItems:'center'}}>
    //                 <View>
    //                     <Image source={require('../../assets/images/foto.jpg')} style={{width:140, height:140, borderRadius:140/2, marginBottom:20}}></Image>
    //                     {/* <Image source={{uri: `data:image/png;base64,${users[0].g_foto}`}} style={{width:140, height:140, borderRadius:140/2, marginBottom:20}}></Image> */}
    //                 </View>
    //                 <View style={{marginBottom:40}}>
    //                     <Text style={styles.titleHead}>{props.nama}</Text>
    //                     <View style={{flexDirection:'row', alignSelf:'center'}}>
    //                         <Text style={styles.subtitleHead}>{props.status}</Text>
    //                         <Text style={styles.subtitleHead}>{props.kelas}</Text>
    //                     </View>
    //                 </View>
    //             </View>
    //         </View>
    //     );
    // }
    // const userFoto = () => {console.log(users)}
    const clearAll = async () => {
        try {
          await AsyncStorage.clear()
        } catch(e) {
          // clear error
        }
        navigation.replace(props.screenName)
        console.log('Done.')
      }
    return (
        <View style={{flex: 1, backgroundColor: '#f8f8f8'}}>
            <Header title="Profil Pengguna"/>
            <View style={{flex:1, marginHorizontal:30}}>
                <View style={{alignItems:'center'}}>
                    {/* {userFoto} */}
                    <Image source={{uri: `data:image/png;base64,${users[0].g_foto}`}} style={{width:145, height:145, borderRadius:145/2, marginVertical:20}}></Image>
                    
                    <Text style={{fontFamily:'ProductSans-Black', fontSize:20, color:'#1A4709'}}>{jk+" "+users[0].g_nm_panggilan}</Text>
                    <Text style={{fontFamily:'ProductSans-Medium', fontSize:16, color:'#4E7934'}}>{users[0].g_nm_lengkap}</Text>
                </View>
                <View style={{marginVertical:20}}>
                    <Text style={{fontFamily:'ProductSans-Reguler', fontSize:14, textAlign:'center'}}>Muh. Zakaria Bagas Rizqi Firdaus</Text>
                    <Text style={{fontFamily:'ProductSans-Reguler', fontSize:14, textAlign:'center'}}>Laki-laki</Text>
                    <Text style={{fontFamily:'ProductSans-Reguler', fontSize:14, textAlign:'center'}}>Surabaya,03 September 1998</Text>
                    <Text style={{fontFamily:'ProductSans-Reguler', fontSize:14, textAlign:'center'}}>Jl. Gading Karya 5 no. 70, Surabaya</Text>
                    <Text style={{fontFamily:'ProductSans-Reguler', fontSize:14, textAlign:'center'}}>0838-3034-3368</Text>
                </View>
                <View>
                    <TouchableOpacity style={styles.tombol} onPress={() => console.log(users)}>
                        <View>
                            <Text style={styles.tombolInside}>Ubah Data</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity style={styles.tombol} onPress={() => {navigation.replace('Login'); clearAll();}}>
                        <View>
                            <Text style={styles.tombolInside}>Keluar</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default index

const styles = StyleSheet.create({
    tombol: {
        marginTop:10,
        backgroundColor:'#1A4709',
        alignItems:'center',
        padding:10,
        borderRadius:16
    },
    tombolInside: {
        fontFamily:'ProductSans-Medium',
        fontSize:16,
        color:'white'
    }
});