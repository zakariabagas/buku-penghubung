import React, { useState } from 'react';
import { Text, View, Image, Dimensions, FlatList, TextInput } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import Header from '../../../organisms/header';
import GambarUserCatatan from '../../../molecules/catatanuser';

import {RadioButton} from 'react-native-paper';

import chatRooms from '../../../../json-server/ChatRooms';
import chats from '../../../../json-server/Chats';
import moment from 'moment';

const index = () => {
    const lebarLayar = Dimensions.get('window').width;
    const tinggiLayar = Dimensions.get('window').height;

    // const listUser = chatRooms.map(x => <GambarUserCatatan selected={true} chatRoom={x}/>);
    const [selected, setSelected] = useState();

    const handleToggle = (terpilih) => {
        // console.warn('Siapa yang tertekan? ', terpilih);
        // console.warn('apakah ini benar ter select?', terpilih );
        setSelected(terpilih);
    };
    const catatan = (x) => {
        setNameCatatan(x.users[1].name);
        setFotoCatatan(x.users[1].imageUri);
        setPesanId(x.id);
        // console.warn('ID berapa? ',x.id);
    };
    const [nameCatatan, setNameCatatan] = useState('');
    const [fotoCatatan, setFotoCatatan] = useState('');
    const [pesanId, setPesanId] = useState('');

    const ProfilHeadBar = () => {
        return(
            <View>
                <View style={{alignItems:'center', flexDirection:'row'}}>
                    {
                        fotoCatatan!==''?
                        <Image source={{uri:fotoCatatan}} style={{width:50, height:50, borderRadius:50/2}} /> :
                        <Image source={require('../../../assets/images/foto.jpg')} style={{width:50, height:50, borderRadius:50/2}} />
                    }
                    {
                        nameCatatan!==''?
                        <Text style={{fontFamily:'ProductSans-Medium', fontSize:14, marginLeft:5}}>{nameCatatan}</Text> : 
                        <Text style={{fontFamily:'ProductSans-Medium', fontSize:14, marginLeft:5}}>Bagas</Text>
                    }
                </View>
                {/* <View style={{alignItems:'center', flexDirection:'row'}}>
                    <Image source={require('../../../assets/images/foto.jpg')} style={{width:50, height:50, borderRadius:50/2}} />
                    <Text style={{fontFamily:'ProductSans-Medium', fontSize:14, marginLeft:5}}>Bagas</Text>
                </View> */}
            </View>
        )
    };

    // const onClickHandler = (i,x) => {
    //     handleToggle({i});
    //     console.warn(`Clicked on `{x}`?`);
    //     chatroom={x};
    // }

    const dataCatatan = chatRooms.map((x,i) => {
        return (
            <GambarUserCatatan 
                onClick={
                    () => {
                        handleToggle(i);
                        // console.warn(`Clicked on ${x.users[1].name}`);
                        catatan(x);
                    }
                } 
                selected={ selected==i ? true : false }
                chatRoom={x}
            />
        ); 
    });

    const momentData = (date) => {
        return(
            // moment(date).format("LLL")
            // moment().diff(moment(date))
            // moment(date).startOf('day').fromNow()
            moment(date).subtract(10, 'days').calendar()
            // moment("20111031", "YYYYMMDD").fromNow(date)
        );
    }

    const pesanCatatan = chats.messages.map((x,i) => {
        switch(chats.id) {
            case pesanId[0] :
                return(
                    <View style={{marginBottom:5}}>
                        <Text>{x.user.name}</Text>
                        <Text>{x.content}</Text>
                        <Text>{momentData(x.createdAt)}</Text>
                        <View style={{height:2, backgroundColor:'black'}} />
                    </View>
                );
            default :
                return (console.warn('PesanId = ', pesanId[0], 'x.id = ',`${chats.id}`));
        }
        /* ({x.id!==pesanId ?
        const data = {
            <View style={{marginHorizontal:10}}>
                <Text style={{textAlign:'center'}}>Belum Terdapat Pesan</Text>
            </View>
        } :
        <View style={{marginBottom:5}}>
            <Text>{x.user.name}</Text>
            <Text>{x.content}</Text>
            <Text>{momentData(x.createdAt)}</Text>
            <View style={{height:2, backgroundColor:'black'}} />
        </View>
        }) */
    });
    
    return (
        <View style={{flex: 1, backgroundColor: '#f8f8f8'}}>
            <Header title="Catatan Khusus"/>
                <View style={{flex:1}}>
                    <ScrollView horizontal={true} style={{backgroundColor: '#4E7934', height:300}}>
                        <View style={{flexDirection:'row', paddingHorizontal:10}}>
                            {/* <FlatList 
                                data={chatRooms} 
                                renderItem={ ({ item }) => <GambarUserCatatan selected={true} chatRoom={item}/> } 
                            /> */}
                            {dataCatatan}
                        </View>
                    </ScrollView>
                    <View style={{backgroundColor: '#f8f8f8', flexDirection:'row', paddingHorizontal:20, paddingVertical:10, justifyContent:'space-between', borderRadius:50,
                        position:'absolute',
                        top:110,
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 1,
                        },
                        shadowOpacity: 0.22,
                        shadowRadius: 2.22,
                            elevation: 3,
                        width:lebarLayar
                    }}>
                        <ProfilHeadBar />
                        <View style={{alignItems:'center', flexDirection:'row'}}>
                            <TouchableOpacity style={{marginRight:5}}>
                                <Text style={{fontFamily:'ProductSans-Regular', fontSize:12}}>Edit</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{marginRight:5}}>
                                <Text style={{fontFamily:'ProductSans-Regular', fontSize:12}}>Hapus</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <ScrollView style={{backgroundColor: '#f8f8f8', marginTop:50, height:tinggiLayar}}>
                        {pesanCatatan}
                        {/* <View style={{marginBottom:5}}>
                        <Text>{x.user.name}</Text>
                        <Text>{x.content}</Text>
                        <Text>{momentData(x.createdAt)}</Text>
                        <View style={{height:2, backgroundColor:'black'}} />
                        </View> */}
                    </ScrollView>
                    <View style={{
                        backgroundColor:'#ffffff',
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 5,
                        },
                        shadowOpacity: 0.34,
                        shadowRadius: 6.27,

                        elevation: 10,
                    }}>
                        <TextInput placeholder="Ketikkan Pesan Disini"/>
                    </View>
                </View>
        </View>
    )
}

export default index
