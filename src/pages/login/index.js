import React, { Component, useEffect, useState } from "react";
import { 
    View,
    Text,
    StyleSheet,
    Button,
    TextInput,
    Image,
    Linking,
    StatusBar
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { ScrollView, TouchableHighlight, TouchableOpacity, TouchableWithoutFeedback } from "react-native-gesture-handler";
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from "axios";
import { Form } from "native-base";

const Login = ({ navigation }) => {
    const [user, setUser] = useState({
        status : "",
        username : "",
        password : ""
    });
    // const [username,setUsername] = useState()
    // const [password,setPassword] = useState()

    const onInputChange = (value, input) => {
        setUser({
            ...user,
            [input]: value,
        })
    }

    useEffect(() => {
        getData()
    }, [])

    const [jsonData, setJsonData] = useState([])
    const jsonValue = async () => {
        Axios.get(`http://192.168.100.10/api/index.php?username=${user.username}&password=${user.password}`)
            .then(async (respon) => {
                console.log(respon)
                if(respon.data && respon.data.length){
                    setJsonData(respon.data)
                    console.warn(respon.data[0].g_nip)
                    await AsyncStorage.setItem('user', respon.data[0].g_nip)
                    await AsyncStorage.setItem('pass', respon.data[0].g_pass)
                    await AsyncStorage.setItem('status', 'guru')

                    alert('Login Guru Berhasil')
                    navigation.replace('HomeGuru')
                } else{
                Axios.get(`http://192.168.43.65/api/dataSiswa.php?username=${username}&password=${password}`)
                    .then(async (responSiswa) => {
                        console.log(responSiswa)
                        if(responSiswa.data && responSiswa.data.length) {
                            // setJsonData(respon.data)
                            // console.warn(respon.data[0].g_nip)
                            // await AsyncStorage.setItem('user', respon.data[0].g_nip)
                            // await AsyncStorage.setItem('pass', respon.data[0].g_pass)
                            // await AsyncStorage.setItem('status', 'guru')
                            alert('Login Siswa Berhasil')
                            // navigation.replace('HomeGuru')
                        } else {
                            alert('Login Gagal Username & Password Salah')
                        }
                    }) .catch(err => {
                        alert ('Login Gagal')
                    })}
            })
            .catch(err => {
                alert(err)
            })
        // return jsonData
    }
    const onRemove = async () => {

    }

    
    const getData = async () => {
        console.warn(await AsyncStorage.getItem('user'))
        // console.warn(jsonData)
    }

    return(
        <View style={styles.container}>
            <StatusBar translucent backgroundColor="transparent" />
            <LinearGradient colors={['#4E7934','#1A4709']} style={{width:'150%',height:'65%', alignSelf:'center', borderRadius:2000, marginTop:-50 }} />
            <View style={{alignItems:'center', top:70, alignSelf:'center', position:"absolute"}}>
                <Image source={require('../../assets/images/logo-sdit.png')} style={{width:80, height:80, marginBottom: 20}}/>      
                <Text style={{ fontFamily:'ProductSans-Black',fontSize:15, color:'white', textAlign:'center'}}>BUKU PENGHUBUNG</Text>
                <Text style={{ fontFamily:'ProductSans-Light',fontSize:12, color:'white', textAlign:'center'}}>SDIT Ulil Albab Kamal</Text>
            </View>
            <View style={{paddingVertical:20, paddingHorizontal:'15%',left:'10%', right:'10%', backgroundColor:'#F8F8F8', borderRadius:30, position:"absolute", alignSelf:'center', top:'38%',
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 5,
                },
                shadowOpacity: 0.34,
                shadowRadius: 6.27,
                
                elevation: 10,
            }}>
                <Text style={ styles.heading }>
                    Silahkan Masuk
                </Text>
                <Text>{user.username}</Text>
                <Text style={{textAlign:'center',}}>{user.status}</Text>
                <View style={{marginBottom:10}}>
                    <TextInput 
                        style={styles.fonetik}
                        placeholder="Username" placeholderTextColor="#C7D8BD" value={user.username} onChangeText={value => onInputChange(value, 'username')} />
                    <TextInput style={styles.fonetik} placeholder="Password" placeholderTextColor="#C7D8BD" value={user.password} onChangeText={value => onInputChange(value, 'password')} />
                </View>
                <TouchableHighlight activeOpacity={0.8} underlayColor="seagreen" style={styles.tombol} 
                    // onPress={() => navigation.replace('HomeGuru')}
                    onPress={jsonValue}
                >
                    <View >
                        <Text style={styles.tombolInside}>Simpan</Text>
                    </View>
                </TouchableHighlight>
                <TouchableWithoutFeedback 
                    onPress={() => {alert('Silahkan Hubungi Admin 083830343368');}}
                >
                    <Text style={styles.lupa}>Lupa Password?</Text>
                </TouchableWithoutFeedback>
            </View>
        </View>
    );
}
export default Login;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'#4E7934',
    },
    fonetik:{
        fontFamily:'ProductSans-Medium',
        fontSize:14,
        paddingBottom:2,
        marginBottom:12,
        color:'#4E7934',
        borderBottomColor:'#C7D8BD',
        borderBottomWidth:1.5
    },
    heading: {
        fontFamily:'ProductSans-Black',
        fontSize:18,
        textAlign:'center',
        marginVertical:20
    },
    lupa: {
        color:'#4E7934',
        textAlign:'center',
        fontFamily:'ProductSans-Light',
        fontSize:12,
        marginVertical:16
    },
    tombol: {
        marginTop:10,
        backgroundColor:'#1A4709',
        alignItems:'center',
        padding:10,
        borderRadius:16
        
    },
    tombolInside: {
        fontFamily:'ProductSans-Medium',
        fontSize:16,
        color:'white'
    }
});