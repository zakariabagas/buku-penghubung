import HomeGuru from './guru';
import Login from './login';
import Splash from './splash';
import Isievaluasi from './guru/isievaluasi';
import Catatankhusus from './guru/catatankhusus';
import Siswa from './guru/siswa';
import User from './guru/user';
import Laporan from './guru/laporan';

export { HomeGuru, Login, Splash, Isievaluasi, Catatankhusus, Siswa, User, Laporan };