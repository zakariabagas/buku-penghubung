import React, { Component } from "react";
import { 
    View,
    Text,
    StyleSheet,
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import Icon from "../../atoms/svg";
import { useNavigation } from '@react-navigation/native'

const index = (props) => {
    const navigation = useNavigation();
    return (
        <View style={styles.header}>
            <View style={styles.head}>
                <View style={{flex:1, justifyContent:'center'}}>
                    <TouchableOpacity style={{}} onPress={() => navigation.goBack()}>
                        <Icon name={'kembali'} height={20} width={15} fill={'#fff'} />
                    </TouchableOpacity>
                </View>
                <View style={{flex:11, alignItems:'center', justifyContent:'center'}}>
                <Text style={{fontFamily:'ProductSans-Black',fontSize:14, color:'white', textAlign:'left', textAlign:'center'}}>{props.title}</Text>
                </View>
            </View>
        </View>
    );
}

export default index

const styles = StyleSheet.create({
    header: {
        paddingTop:25,
        backgroundColor: '#1A4709'      
    },
    head: {
        paddingVertical:15,
        flexDirection: 'row',
        marginHorizontal:25
    },
})
