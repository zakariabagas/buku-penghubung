import React from 'react'
import { View } from 'react-native'
import NavIcon from '../../molecules/navicon'

const NavBar = (props) => {
        switch (props.user) {
            case "guru":
                return(
                    <View style={{height:80,flexDirection:'row',backgroundColor:'#1A4709'}}>
                        <NavIcon namaIcon={"beranda"} teks={"Beranda"} halaman={'beranda'}/>
                        <NavIcon namaIcon={"isi buku penghubung"} teks={"Isi Evaluasi"} halaman={'Isievaluasi'} />
                        <NavIcon namaIcon={"catatan khusus"} teks={"Catatan Khusus"} halaman={'Catatankhusus'} />
                        <NavIcon namaIcon={"siswa"} teks={"Data Siswa"} halaman={'Siswa'} />
                        <NavIcon namaIcon={"guru"} teks={"Pengguna"} halaman={'Guru'}/>
                    </View>
                );
            case "siswa":
                return(
                    <View style={{height:80,flexDirection:'row',backgroundColor:'#1A4709'}}>
                        <NavIcon namaIcon={"beranda"} teks={"Beranda"} />
                        <NavIcon namaIcon={"isi buku penghubung"} teks={"Isi Evaluasi"} />
                        <NavIcon namaIcon={"catatan khusus"} teks={"Catatan Khusus"} />
                        <NavIcon namaIcon={"guru"} teks={"Data Guru"} />
                        <NavIcon namaIcon={"siswa"} teks={"Pengguna"} />
                    </View>
                );
            default:
            return;
          }
}

export default NavBar
