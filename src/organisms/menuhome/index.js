import React from 'react'
import { View } from 'react-native'
import Menuicon from '../../molecules/menuicon'

const NavBar = (props) => {
        switch (props.user) {
            case "guru":
                return(
                    <View style={{flexDirection: 'row', flexWrap:'wrap', marginHorizontal:30, marginVertical:20}}>
                        <View style={{justifyContent: 'space-between', flexDirection:'row', width: '100%', flexWrap:"wrap"}}>
                            <Menuicon nama="Isi Buku Penghubung" iconName="isi buku penghubung" replace={false} screenName={'Isi Evaluasi'}/>
                            <Menuicon nama="Catatan Khusus" iconName="catatan khusus" replace={false} screenName={'Catatan Khusus'}/>
                            <Menuicon nama="Lihat Data Siswa" iconName="siswa" replace={false} screenName={'Data Siswa'}/>
                            <Menuicon nama="Cek Profil Guru" iconName="guru" replace={false} screenName={'Pengguna'}/>
                            <Menuicon nama="Lihat Laporan" iconName="laporan" replace={false} screenName={'Laporan'}/>
                            <Menuicon nama="Keluar" iconName="keluar" replace={true} screenName={'Login'}/>
                        </View>
                    </View>
                );
            case "siswa":
                return(
                    <View style={{height:80,flexDirection:'row',backgroundColor:'#1A4709'}}>
                        <NavIcon namaIcon={"beranda"} teks={"Beranda"} />
                        <NavIcon namaIcon={"isi buku penghubung"} teks={"Isi Evaluasi"} />
                        <NavIcon namaIcon={"catatan khusus"} teks={"Catatan Khusus"} />
                        <NavIcon namaIcon={"guru"} teks={"Data Guru"} />
                        <NavIcon namaIcon={"siswa"} teks={"Pengguna"} />
                    </View>
                );
            default:
            return;
          }
}

export default NavBar
