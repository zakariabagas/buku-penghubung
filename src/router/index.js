import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { HomeGuru, Splash, Login, Isievaluasi, Siswa, Catatankhusus, User, Laporan } from '../pages'
import Icon from '../atoms/svg';

const Stack = createStackNavigator();

const Tab = createBottomTabNavigator();

/* const tabBarOptions = () => {
    //activeTintColor:"#FEFEFE", 
    //style: {
    //   backgroundAColor:"#1A4709", }
        showLabel: false,
        tabStyle: {
          backgroundColor: "#343434";
          borderTopColor: "#343434";
          paddingBottom: 10
        }
} */

const HomeStack = createStackNavigator();
function HomeStackScreen() {
 return (
   <HomeStack.Navigator headerMode="none">
        <HomeStack.Screen name="Home" component={HomeGuru} />
        <HomeStack.Screen name="Laporan" component={Laporan} />
   </HomeStack.Navigator>
  );
}

const TabsGuru = () => {
    return(
        <Tab.Navigator initialRouteName="Beranda" tabBarOptions={{activeTintColor: '#FFFF01', inactiveTintColor: '#FEFEFE', tabStyle:{backgroundColor:'#1A4709'}}}>
            <Tab.Screen name="Beranda" component={HomeStackScreen} options={{
                headerShown:false,
                tabBarIcon: ({ color }) => (
                    <Icon name="beranda" fill={color} height={30} width={30} />
                ),
            }} />
            <Tab.Screen name="Isi Evaluasi" component={Isievaluasi} options={{
                headerShown:true,
                tabBarIcon: ({ color }) => (
                    <Icon name="isi buku penghubung" fill={color} height={30} width={30} />
                ),
            }} />
            <Tab.Screen name="Catatan Khusus" component={Catatankhusus} options={{
                headerShown:false,
                tabBarIcon: ({ color }) => (
                    <Icon name="catatan khusus" fill={color} height={30} width={30} />
                ),
                tabBarBadge: '3'
            }} />
            <Tab.Screen name="Data Siswa" component={Siswa} options={{
                headerShown:false,
                tabBarIcon: ({ color }) => (
                    <Icon name="siswa" fill={color} height={28} width={28} />
                ),
            }} />
            <Tab.Screen name="Pengguna" component={User} options={{
                headerShown:false,
                tabBarIcon: ({ color }) => (
                    <Icon name="guru" fill={color} height={30} width={30} />
                ),
            }} />
        </Tab.Navigator>
    )
};

const Router = () => {
    return(
    <Stack.Navigator initialRouteName='Splash'>
        <Stack.Screen name="Splash" component={Splash} 
            options={{
                headerShown:false,
                tabBarIcon: ({ color, size }) => (
                    <MaterialCommunityIcons name="home" color={color} size={size} />
                ),
            }}
        />
        <Stack.Screen name="Login" component={Login} 
            options={{
                headerShown:false,
            }}
        />
        <Stack.Screen name="HomeGuru" component={TabsGuru} 
            options={{
                headerShown:false,
            }}
        />
    </Stack.Navigator>
    )
}
export default Router;