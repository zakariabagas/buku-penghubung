import { combineReducers } from "redux";
import { SET_API } from "./types";

const initialState = {
    name: 'Zakaria Bagas'
}

const stateReducer = (state = initialState, action) => {
    return state;
};

const initialLoginDataGuruSet = {
    // g_nip: '',
    // g_user: '',
    // g_pass: '',
    // g_nm_lengkap: '',
    // g_nm_panggilan: '',
    // g_jk: '',
    // g_tmp_lahir: '',
    // g_tgl_lahir: '',
    // g_alamat: '',
    // g_telepon: '',
    // g_foto: '',
    user: []
}

const initialUserPassSet = {
    user: '',
    pass: ''
}

const userPassReducer = ( state = initialUserPassSet, action ) => {
    if(action.type === 'SET_USER_PASS') {
        return{
            ...state,
            [action.inputType]: action.inputValue,
        }
    }
    return state;
}

const loginDataGuruSetReducer = (state = initialLoginDataGuruSet, action ) => {
    if(action.type === 'SET_FORM') {
        return {
            ...state,
            [action.inputType]: action.inputValue,
        }
    }
    if(action.type === SET_API) {
        return {
            ...state,
            user: action.payload
            // ['action.inputType']: action.inputValue,
        }
    }
    return state;
};

const reducer = combineReducers({
    stateReducer,
    loginDataGuruSetReducer,
    userPassReducer,
});

export default reducer;