import Axios from "axios";
import { SET_API } from "./types";

export const setForm = (inputType, value) => {
    return {type: 'SET_FORM', inputType: inputType, inputValue: value};
};
const urlAPI = 'http://192.168.43.65/api/index.php?username=180631100016&password=muhtarom'
export const setApi = (userApi) => {
    return {
        type: SET_API,
        payload: userApi
    };
};
export const setApiAction = () => {
    return dispatch => {
        return Axios.get(urlAPI)
            .then(response => {
                dispatch (setApi([response.data[0]]));
                console.log('ini hasil setApiAction : ',response.data[0]);
            })
            .catch(err => {
                console.log(err);
                throw err;
            });
    };
};
