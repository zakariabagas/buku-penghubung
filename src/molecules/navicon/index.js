import React from "react";
import { 
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from "react-native";
import Icon from "../../atoms/svg";

const NavIcon = (props, {navigation}) => {
    return(
        <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
            <TouchableOpacity style={{alignItems:"center"}} onPress={() => alert(props.halaman)}>
                <Icon name={props.namaIcon} height={30} width={30} />
                <Text style={styles.teksMenu}>{props.teks}</Text>
            </TouchableOpacity>
        </View>
    )
}

export default NavIcon;

const styles = StyleSheet.create({
    teksMenu: {
        fontFamily:'ProductSans-Reguler',
        fontSize:10,
        color:'white',
        textAlign:'center',
        marginTop:5,
    }
});