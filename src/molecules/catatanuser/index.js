import React, { useState } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'

const index = (props) => {

    const { chatRoom } = props;

    const user = chatRoom.users[1];

    const onClick = () => {
        // console.warn(`Clicked on ${user.name}`);
        
    }

    return (
        <TouchableOpacity onPress={props.onClick} style={props.selected  ? styles.boxSelect : styles.boxUnselect}>
            <View style={{alignItems:'center', }}>
                <Image source={{uri: user.imageUri}} style={{width:40, height:40, borderRadius:40/2}} />
                <View style={{flexDirection:'row', width:50, justifyContent:'center'}}>
                    <Text numberOfLines={2} ellipsizeMode='tail' style={{flexShrink: 1,fontFamily:'ProductSans-Medium', fontSize:10, textAlign:'center', marginTop:5}}>{user.name}</Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}

export default index

const styles = StyleSheet.create({
    boxSelect: {
        alignItems:'center', 
        borderRadius:15,
        height:90,
        paddingHorizontal:5, 
        paddingTop:10, 
        marginTop:10, 
        marginHorizontal:2,
        backgroundColor:'#1A4709'
    },
    boxUnselect: {
        alignItems:'center', 
        borderRadius:15,
        height:90,
        paddingHorizontal:5, 
        paddingTop:10, 
        marginTop:10, 
        marginHorizontal:2,
    }
})

//INI KODINGAN AWAL



// import React from 'react'
// import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'

// const index = (props) => {
//     return (
//         <TouchableOpacity>
//             <View style={[props.selected ? {backgroundColor:'#1A4709'} : null , {alignItems:'center', borderRadius:15, height:90, paddingHorizontal:5, paddingTop:10, marginTop:10, marginHorizontal:2}]}>
//                 <Image source={require('../../assets/images/foto.jpg')} style={{width:40, height:40, borderRadius:40/2}} />
//                 <View style={{flexDirection:'row', width:50}}>
//                     <Text numberOfLines={2} ellipsizeMode='tail' style={{flexShrink: 1,fontFamily:'ProductSans-Medium', fontSize:10, textAlign:'center', marginTop:5}}>bagas sasas as as ssss</Text>
//                 </View>
//             </View>
//         </TouchableOpacity>
//     )
// }

// export default index

// const styles = StyleSheet.create({})

