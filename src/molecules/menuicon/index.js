import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native'
import React from 'react'
import { StyleSheet, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from '../../atoms/svg';

const index = (props) => {
    const navigation = useNavigation();
    if(props.replace == true) {
        const clearAll = async () => {
            try {
              await AsyncStorage.clear()
            } catch(e) {
              // clear error
            }
            navigation.replace(props.screenName)
            console.log('Done.')
          }
        return (
            <View style={{width: '50%', alignItems:'center', justifyContent:'center', height:100}}>
                <TouchableOpacity style={{alignItems:'center'}} onPress={clearAll}>
                    <Icon name={props.iconName} height={50} width={100} fill={'#1A4709'} />
                    <View style={{}}>
                        <Text style={{fontFamily:'ProductSans-Medium', fontSize:14, color:'#1A4709', textAlign:'center', marginTop:10,}}>{props.nama}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    } else {
        return (
            <View style={{width: '50%', alignItems:'center', justifyContent:'center', height:100}}>
                <TouchableOpacity style={{alignItems:'center'}} onPress={() => navigation.navigate(props.screenName)}>
                        <Icon name={props.iconName} height={50} width={100} fill={'#1A4709'} />
                        <View style={{}}>
                            <Text style={{fontFamily:'ProductSans-Medium', fontSize:14, color:'#1A4709', textAlign:'center', marginTop:10,}}>{props.nama}</Text>
                        </View>
                </TouchableOpacity>
            </View>
        );
    }
}

export default index

const styles = StyleSheet.create({})
