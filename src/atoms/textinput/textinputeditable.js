import React from 'react'
import { StyleSheet, TextInput, View } from 'react-native';

const TextInputEditable = (props) => {
    return(
        <TextInput 
            style={[styles.fonetikBenar, {...props.styleText}]}  
            editable={false} selectTextOnFocus={false}
            placeholder={props.placeHolder} placeholderTextColor="#707070" 
            {...props}
            />
    )
};

export default TextInputEditable

const styles = StyleSheet.create({
    fonetikBenar: {
        fontFamily:'ProductSans-Medium',
        fontSize:11,
        borderColor:'#1A4709',
        color:'#4E7934',
        borderBottomWidth:1,
        paddingVertical:5,
        paddingHorizontal:10,
        marginTop:5
    },
})
