import React from 'react';
import { View, Text } from 'react-native';
import TextInputEditable from './textinputeditable.js';
import TextInputNonEditable from './textinputnoneditable.js';


const Textinput = props => {
    switch (props.editable) {
        case true :
            return <TextInputEditable styleText={props.style} placeHolder={props.placeholder} {...props} />
        case false :
            return <TextInputNonEditable styleText={props.style} placeHolder={props.placeholder} {...props} />
        default :
            return
    }
}

export default Textinput
