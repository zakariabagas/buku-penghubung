import React from 'react'
import { StyleSheet, TextInput, View } from 'react-native';

const TextInputNonEditable = (props) => {
    return(
        <TextInput 
            style={[styles.fonetikSalah, {...props.styleText}]}  
            editable={false} selectTextOnFocus={false}
            placeholder={props.placeHolder} placeholderTextColor="#707070" 
            {...props}  
            />
    )
};

export default TextInputNonEditable

const styles = StyleSheet.create({
    fonetikSalah: {
        fontFamily:'ProductSans-Medium',
        fontSize:11,
        borderColor:'#707070',
        borderBottomWidth:1,
        backgroundColor:'#C5C5C5',
        paddingVertical:5,
        paddingHorizontal:10,
        marginTop:5
    },
})
