import * as React from "react"
import Svg, { Defs, G, Path } from "react-native-svg"
/* SVGR has dropped some elements not supported by react-native-svg: style */

function SvgComponent(props) {
  return (
    <Svg
      width={233.025}
      height={178.199}
      viewBox="0 0 7947 6077"
      shapeRendering="geometricPrecision"
      textRendering="geometricPrecision"
      imageRendering="optimizeQuality"
      fillRule="evenodd"
      clipRule="evenodd"
      fill={"#FEFEFE"}
      {...props}
    >
      <Defs></Defs>
      <G id="prefix__Layer_x0020_1">
        <G id="prefix___359938416">
          <Path
            className="prefix__fil0"
            d="M2870 2168c-134-606 723-750 827-210 117 605-726 672-827 210zm-1623 0c-137-601 742-756 842-200 105 585-735 668-842 200zm3227-54c-68-596 809-639 848-105 40 555-790 613-848 105zM820 4273l-223 936c507-189 1348-911 1887-925 927-25 1908-6 2840 0 1554 11 1208-449 1217-3043 2-556 32-815-365-1069C5757-97 4157 30 3524 30c-558 0-2399-71-2812 34C-206 295 26 1225 26 2223c0 438-30 997 3 1420 32 409 375 589 791 630z"
          />
          <Path
            className="prefix__fil0"
            d="M2436 4644c-231 1402 3207 393 4020 845 356 197 729 462 1026 588l-124-635c743-377 570-427 570-2020 0-1171 209-1850-746-1773-209 1897 504 3001-898 2994-1268-6-2585-32-3848 1z"
          />
        </G>
      </G>
    </Svg>
  )
}

export default SvgComponent