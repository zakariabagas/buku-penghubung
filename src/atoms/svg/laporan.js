import * as React from "react"
import Svg, { Defs, G, Path } from "react-native-svg"
/* SVGR has dropped some elements not supported by react-native-svg: style */

function SvgComponent(props) {
  return (
    <Svg
      width={180.375}
      height={180.13}
      viewBox="0 0 575 574"
      shapeRendering="geometricPrecision"
      textRendering="geometricPrecision"
      imageRendering="optimizeQuality"
      fillRule="evenodd"
      clipRule="evenodd"
      fill={"#FEFEFE"}
      {...props}
    >
      <Defs></Defs>
      <G id="prefix__Layer_x0020_1">
        <G id="prefix___355804440">
          <Path
            className="prefix__fil0"
            d="M1 2v447c0 35-5 31 14 50 22 22 42 42 63 64 7 7 8 10 20 11l351-1v-39c-39 6-309 9-340 3l-1-70H35l1-433h378v215l35 5V0L1 2z"
          />
          <Path
            className="prefix__fil0"
            d="M381 324c91-21 124 121 29 141-91 19-123-120-29-141zm108 124c30-53 7-117-28-141-47-33-106-26-141 8-45 44-41 131 16 167 47 31 79 16 115 5l86 85c8-7 18-20 38-34-10-20-60-58-86-90zM342 250h36V71l-11-1-25 1zM73 321l179 1v-35H72zM72 376h180v-36l-180 2zM72 430h180v-36l-179 2zM234 250h36V107l-36-1zM180 250h36V143h-35zM288 250h36V143h-35zM126 250h36v-71l-36-1zM74 250l33 1v-70H74z"
          />
          <Path
            className="prefix__fil0"
            d="M74 250v-69h33v70c4-10 3-66 0-73H73l-2 64c1 6 0-1 1 4l2 4z"
          />
        </G>
      </G>
    </Svg>
  )
}

export default SvgComponent
