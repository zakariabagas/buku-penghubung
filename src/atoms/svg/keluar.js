import * as React from "react"
import Svg, { Defs, G, Path } from "react-native-svg"
/* SVGR has dropped some elements not supported by react-native-svg: style */

function SvgComponent(props) {
  return (
    <Svg
      width={217.641}
      height={203.667}
      viewBox="0 0 1295 1211"
      shapeRendering="geometricPrecision"
      textRendering="geometricPrecision"
      imageRendering="optimizeQuality"
      fillRule="evenodd"
      clipRule="evenodd"
      fill={"#FEFEFE"}
      {...props}
    >
      <Defs></Defs>
      <G id="prefix__Layer_x0020_1">
        <G id="prefix___314858856">
          <Path
            className="prefix__fil0"
            d="M328 506c42-11 78 23 85 55 9 44-20 78-55 86-88 20-129-115-30-141zM0 296v652c0 35 12 66 31 81 13 9 26 9 41 16l194 88c51 24 103 46 154 70 17 8 22 13 40 3 45-26 37-99 37-143V101c0-37-5-66-25-86-7-7-23-19-37-14-14 6-26 12-39 18L89 159c-16 7-22 11-40 15-15 4-26 15-32 25-19 29-17 57-17 97zM1011 436H685v302l326 1v121c7-3 218-213 248-242 8-8 31-28 36-36-4-7-132-130-141-139-11-11-133-133-143-138v131z"
          />
          <Path
            className="prefix__fil0"
            d="M865 1009l-262 1-1 96c54 3 113 0 167 0h85c27 0 47-7 62-21 37-35 33-75 33-135 0-26 1-154-1-168h-82l-1 227zM602 202h263v176l84 1V224c0-27 0-44-11-67-8-18-20-32-36-41-22-12-38-11-66-11h-78c-50 0-105-2-155 0l-1 97z"
          />
        </G>
      </G>
    </Svg>
  )
}

export default SvgComponent
