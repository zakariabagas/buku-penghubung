import * as React from "react"
import Svg, { Defs, G, Path } from "react-native-svg"
/* SVGR has dropped some elements not supported by react-native-svg: style */

function SvgComponent(props) {
  return (
    <Svg
      width={159.451}
      height="55.24mm"
      viewBox="0 0 449 588"
      shapeRendering="geometricPrecision"
      textRendering="geometricPrecision"
      imageRendering="optimizeQuality"
      fillRule="evenodd"
      clipRule="evenodd"
      fill={"#FEFEFE"}
      {...props}
    >
      <Defs></Defs>
      <G id="prefix__Layer_x0020_1">
        <G id="prefix___359926584">
          <Path
            className="prefix__fil0"
            d="M118 280c-26 26-58 53-74 104-46 139 29 147 45 129 9-18 0-69 4-96 3-17 15-69 36-54 17 14 1 18-3 45 47 0 65 11 97 29 39-16 26-26 100-29-4-23-15-24-8-46 56-14 36 129 44 153 100 31 66-168-34-240-29 28-66 44-106 44-38 0-73-14-101-39zM337 57C311 22 269 0 219 0 163 1 118 31 93 73c-6 18-9 37-9 58 0 44 15 84 40 113 24 28 58 45 95 45 36 0 70-17 94-45 25-29 40-69 40-113 0-27-6-52-16-74z"
          />
          <Path
            className="prefix__fil0"
            d="M15 521c-9 13-16 49-15 67 30-9 68-22 102-27 65-9 85 18 108 20 0-19 2-105-2-116-8-22-59-29-88-28-6 33 14 83-17 102-33 22-67 0-88-18zM238 462v120c40-13 52-28 107-21 36 4 72 18 104 27l-13-69c-18 15-52 41-88 22-34-17-14-63-20-103-38-7-68 10-90 24z"
          />
        </G>
      </G>
    </Svg>
  )
}

export default SvgComponent
