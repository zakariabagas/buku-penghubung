import * as React from "react"
import Svg, { Defs, G, Path } from "react-native-svg"
/* SVGR has dropped some elements not supported by react-native-svg: style */

function SvgComponent(props) {
  return (
    <Svg
      width={272.942}
      height={195.769}
      viewBox="0 0 2574 1846"
      shapeRendering="geometricPrecision"
      textRendering="geometricPrecision"
      imageRendering="optimizeQuality"
      fillRule="evenodd"
      clipRule="evenodd"
      fill={"#FEFEFE"}
      {...props}
    >
      <Defs></Defs>
      <G id="prefix__Layer_x0020_1">
        <G id="prefix___361704544">
          <Path
            className="prefix__fil0"
            d="M187 190c27-10 59-29 91-42 100-41 216-65 328-64 114 0 341 50 412 112v1170c-66-13-121-34-196-46-144-22-297-21-440 3-72 13-130 33-196 46l1-1179zm1369 1117c-90-14-260 10-354 32-27 7-78 24-101 25V196c29-27 141-65 186-79 200-64 490-29 646 76v684c0 41 3 32 13 39 6-26 109-114 137-142 22-22 35-25 36-63 2-118 6-270-1-387-24-13-53-43-77-62-36-28-25-40-26-116-271-157-546-196-848-75-142 57-62 67-209 3-37-16-74-29-115-40C752 10 668-1 568 2 405 7 202 70 104 149l-1 90c-32 34-73 57-103 91v1317l1314 1c33-43 63-158 97-196 23-25 123-131 145-145z"
          />
          <Path
            className="prefix__fil0"
            d="M1435 1470c12 12 13 9 32 15 55 17 48 64 62 81l31 13c13 4 21 8 32 17 24 20 19 32 32 63l108-106c36-38 68-72 104-110l158-164c18-20 36-34 54-53 70-73 139-144 211-218 18-20 34-36 52-56 47-52 95-92 158-164-41-36-155-161-191-183-115 111-311 317-420 431-70 74-141 145-210 219l-213 215zM1682 1648h435l1-459c-20 13-88 87-109 110-43 48-316 325-327 349zM2297 588l190 181c120-114 106-114 16-202-25-24-60-81-110-69-24 6-83 69-96 90z"
          />
          <Path
            className="prefix__fil0"
            d="M1391 1765c-27-39-13-25-50-52l85-199c55 5 65 34 71 91 54 5 87 11 92 67l-198 93zm-132 81l348-166c1-64-33-83-93-92-15-62-29-89-98-93l-157 351z"
          />
          <Path
            className="prefix__fil0"
            d="M1328 1721l95-217c51 2 79 34 83 95 60-1 90 25 90 78l-213 101-55-57z"
          />
        </G>
      </G>
    </Svg>
  )
}

export default SvgComponent
