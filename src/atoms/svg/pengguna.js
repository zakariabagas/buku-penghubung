import * as React from "react"
import Svg, { Defs, G, Path } from "react-native-svg"
/* SVGR has dropped some elements not supported by react-native-svg: style */

function SvgComponent(props) {
  return (
    <Svg
      width={179.495}
      height={179.575}
      viewBox="0 0 1486 1486"
      shapeRendering="geometricPrecision"
      textRendering="geometricPrecision"
      imageRendering="optimizeQuality"
      fillRule="evenodd"
      clipRule="evenodd"
      fill={"#FEFEFE"}
      {...props}
    >
      <Defs></Defs>
      <G id="prefix__Layer_x0020_1">
        <G id="prefix___359941344">
          <Path
            className="prefix__fil0"
            d="M566 918h-67c-17 0-15-2-27 6l-58 38c-58 39-142 90-201 126-60 37-107 58-155 107-79 80-75 227-2 279 25 18 83 11 120 11 262-2 524 0 786-1l394 1c48 0 77 3 100-26s31-67 29-103c-3-71-38-133-82-179-41-42-181-125-234-158l-122-75c-61-37-22-25-106-25-14 0-19-4-28 3-54 44-185 39-251 30-19-3-37-6-54-11-9-3-29-10-35-16-6-7 2-4-7-7zM1134 483c10-67-7-167-32-226l-14-29C1040 122 929 21 787 3c-60-8-126 1-178 23-66 28-103 59-149 109-59 65-97 163-106 254-15 147 44 300 135 380l35 30c45 31 80 50 132 64 46 11 93 14 141 7 29-5 56-12 82-23 12-5 24-10 35-16 6-4 11-7 17-10l15-10c37-25 53-41 82-73l49-71c1 0 1-1 1-1l39-96c4-14 8-27 11-42l6-45z"
          />
        </G>
      </G>
    </Svg>
  )
}

export default SvgComponent
