import * as React from "react"
import Svg, { Defs, G, Path } from "react-native-svg"
/* SVGR has dropped some elements not supported by react-native-svg: style */

function SvgComponent(props) {
  return (
    <Svg
      width={182.024}
      height={182.024}
      viewBox="0 0 8114 8114"
      shapeRendering="geometricPrecision"
      textRendering="geometricPrecision"
      imageRendering="optimizeQuality"
      fillRule="evenodd"
      clipRule="evenodd"
      fill={"#FEFEFE"}
      {...props}
    >
      <Defs></Defs>
      <G id="prefix__Layer_x0020_1">
        <G id="prefix___356645680">
          <Path
            className="prefix__fil0"
            d="M4455 7279H1194c-187 0-360-172-360-360V1194c0-187 173-360 360-360h5725c188 0 360 173 360 360v5725c0 188-172 360-360 360h-436v-796H4455v796zM0 1327v5460c0 801 525 1327 1327 1327h5460c803 0 1327-527 1327-1327V1327C8114 526 7590 0 6787 0H1327C526 0 0 523 0 1327z"
          />
          <Path
            className="prefix__fil0"
            d="M2654 4588c0-100 36-214 67-293 50-128 108-194 123-257-430-100-764-258-1266 156-161 132-365 320-365 602v834c0 44 13 57 57 57h5573c44 0 57-13 57-57v-834c0-438-664-872-1061-872-244 0-381 70-569 114 19 80 190 293 190 550-110-127-419-948-1403-948-469 0-901 225-1176 587-45 60-74 102-114 170l-113 191zM3242 2408c0 931 1201 1142 1554 379 148-319 75-652-152-910-455-520-1402-172-1402 531zM1630 3014c0 622 642 808 1019 488 319-271 237-844-165-1019-228-99-484-55-660 118-102 101-194 218-194 413zM5270 3014c0 622 642 808 1019 488 241-205 278-625 10-891-409-407-1029-60-1029 403z"
          />
        </G>
      </G>
    </Svg>
  )
}

export default SvgComponent
