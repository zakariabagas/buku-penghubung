import * as React from "react"
import Svg, { Defs, G, Path } from "react-native-svg"
/* SVGR has dropped some elements not supported by react-native-svg: style */

function SvgComponent(props) {
  return (
    <Svg
      width={309.58}
      height={372.477}
      viewBox="0 0 7621 9170"
      shapeRendering="geometricPrecision"
      textRendering="geometricPrecision"
      imageRendering="optimizeQuality"
      fillRule="evenodd"
      clipRule="evenodd"
      fill={"#FEFEFE"}
      {...props}
    >
      <Defs></Defs>
      <G id="prefix__Layer_x0020_1">
        <Path
          className="prefix__fil0"
          d="M2106 4585l3111-3111c337-337 337-884 0-1221-338-337-885-337-1222 0L239 4009c-319 319-319 834 0 1152l3756 3756c337 337 884 337 1222 0 337-337 337-884 0-1221L2106 4585z"
        />
        <Path
          className="prefix__fil0"
          d="M5339 4585l2111-2111c229-229 229-600 0-828-229-229-600-229-829 0L4072 4194c-215 216-215 566 0 782l2549 2548c229 229 600 229 829 0 229-228 229-600 0-828L5339 4585z"
        />
      </G>
    </Svg>
  )
}

export default SvgComponent
