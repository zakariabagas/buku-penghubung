import * as React from "react"
import Svg, { Defs, G, Path } from "react-native-svg"
/* SVGR has dropped some elements not supported by react-native-svg: style */

function SvgComponent(props) {
  return (
    <Svg
      width={221.555}
      height={176.316}
      viewBox="0 0 1168 929"
      shapeRendering="geometricPrecision"
      textRendering="geometricPrecision"
      imageRendering="optimizeQuality"
      fillRule="evenodd"
      clipRule="evenodd"
      fill={"#FEFEFE"}
      {...props}
    >
      <Defs></Defs>
      <G id="prefix__Layer_x0020_1">
        <G id="prefix___355804992">
          <Path
            className="prefix__fil0"
            d="M584 188L167 531v352c0 12 4 23 13 32 10 10 20 14 33 14h278V651h186v278h278c12 0 23-4 32-14 9-9 14-20 14-32V535c0-2 0-3-1-4L584 188z"
          />
          <Path
            className="prefix__fil0"
            d="M1160 453l-159-132V25c0-6-2-12-7-16-4-5-9-7-16-7H839c-7 0-13 2-17 7-4 4-6 10-6 16v142L639 19C623 6 605 0 584 0s-40 6-55 19L8 453c-5 4-7 9-8 15 0 7 1 12 5 17l45 54c4 4 9 7 15 8 6 0 12-1 18-5l501-418 501 418c4 3 9 5 15 5h2c7-1 12-4 16-8l44-54c4-5 6-10 6-17-1-6-4-11-8-15z"
          />
        </G>
      </G>
    </Svg>
  )
}

export default SvgComponent
