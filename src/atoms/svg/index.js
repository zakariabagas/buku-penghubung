import React from "react";

import Admin from "./admin";
import Beranda from "./beranda";
import Catatankhusus from "./catatan khusus";
import Guru from "./guru";
import Isibukupenghubung from "./isi buku penghubung";
import Kelas from "./kelas";
import Laporan from "./laporan";
import Pengguna from "./pengguna";
import Siswa from "./siswa";
import Keluar from "./keluar";
import Kembali from "./kembali";

const Icon = props => {
  switch (props.name) {
    case "beranda":
      return <Beranda {...props} />;
    case "admin":
      return <Admin {...props} />;
    case "catatan khusus":
      return <Catatankhusus {...props} />;
    case "guru":
      return <Guru {...props} />;
    case "isi buku penghubung":
      return <Isibukupenghubung {...props} />;
    case "kelas":
      return <Kelas {...props} />;
    case "laporan":
      return <Laporan {...props} />;
    case "pengguna":
      return <Pengguna {...props} />;
    case "siswa":
      return <Siswa {...props} />;
    case "keluar":
      return <Keluar {...props} />;
    case "kembali":
      return <Kembali {...props} />;
    default:
    return;
  }
};

export default Icon;