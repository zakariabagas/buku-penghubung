import {
    // ...
    Platform
  } from "react-native";
  import iosTabControlStyles from "./iOSTabControlStyles";
  import androidTabControlStyles from "./androidTabControlStyles";
  const isIos = Platform.OS === "ios";
  const wrapperStyles = StyleSheet.create({
    outerGapStyle: isIos ? { padding: theme.spacing.s } : { padding: 0 }
  });
  const tabControlStyles = isIos ? iosTabControlStyles : androidTabControlStyles;
  const TabControl = ({ values, onChange, renderSeparators }) => {
    const [selectedIndex, setSelectedIndex] = useState(0);
    const handleIndexChange = index => {
      setSelectedIndex(index);
      onChange(values[index]);
    };
    return (
      <View style={wrapperStyles.outerGapStyle}>
        <SegmentedControl
          values={values}
          selectedIndex={selectedIndex}
          onIndexChange={handleIndexChange}
          renderSeparators={renderSeparators}
        />
      </View>
    );
  };
  