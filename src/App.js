import React, { Component } from "react";
import { 
  View,
  Text,
  StyleSheet,
  Button
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { TextInput } from "react-native-gesture-handler";
import Router from './router';
import { Provider } from 'react-redux';
import {store} from "./redux";

const Login = ({ navigation }) => {
  return(
    <View style={styles.container}>
      <Text style={styles.heading}>
        Hello
      </Text>
      <View>
        <TextInput placeholder="User"></TextInput>
        <TextInput placeholder="Pass"></TextInput>
        <Button 
          title="Masuk"
          onPress={() => navigation.navigate('HomeGuru')}
        />
      </View>
    </View>
  );
}

function Coba({ navigation }) {
  return(
    <View>
      <Text>COBA</Text>
    </View>
  )
}

const Stack = createStackNavigator();

function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Router />
      </NavigationContainer>
    </Provider>
  );
}
export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  heading: {
    fontSize: 24,
  }
});