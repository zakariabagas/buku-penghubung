import React from 'react';
import {Button, View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

function HomeScreen({ navigation }) {
  return(
    <View style={{flex:1, alignItems:'center',justifyContent:'center'}}>
      <Text>Ini Adalah Home Screen</Text>
      <Button title="Lanjut Screen Berikutnya" onPress={() => navigation.navigate('Lanjutkan')} />
    </View>
  )
}

function Lanjut({ navigation }) {
  return(
    <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
      <Text>Ini Adalah Halaman Lanjut</Text>
      <Button title="ke Lanjut" color="#CC2A49" onPress={() => navigation.push('Lanjutkan')} />
      <Button title="Pop Up" onPress={() => navigation.popToTop()} />
      <Button title="Balik Ke Home" color="#010101" onPress={() => navigation.navigate("Lanjutkan")} />
    </View>
  )
}

const Stack=createStackNavigator();

function App() {
  return(
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} options={{title:'Ini Halaman Home'}}>
        </Stack.Screen>
        <Stack.Screen name="Lanjutkan" component={Lanjut}>
        </Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default App;
