export default [{
    id: '1',
    users: [{
      id: 'u1',
      name: 'Vadim',
      imageUri: 'https://image.ibb.co/k0wVTm/profile_pic.jpg',
    }, {
      id: 'u2',
      name: 'Lukas',
      imageUri: 'https://image.ibb.co/cA2oOb/alex_1.jpg',
    }],
    lastMessage: {
      id: 'm1',
      content: 'Well done this sprint, guys!',
      createdAt: '2020-10-03T14:48:00.000Z',
    }
  }, {
    id: '2',
    users: [{
      id: 'u1',
      name: 'Vadim',
      imageUri: 'https://image.ibb.co/k0wVTm/profile_pic.jpg',
    }, {
      id: 'u3',
      name: 'Daniil',
      imageUri: 'https://image.ibb.co/gSyTOb/bob_1.jpg',
    }],
    lastMessage: {
      id: 'm2',
      content: 'How are you doing?',
      createdAt: '2020-10-02T15:40:00.000Z',
    }
  }, {
    id: '3',
    users: [{
      id: 'u1',
      name: 'Vadim',
      imageUri: 'https://image.ibb.co/k0wVTm/profile_pic.jpg',
    }, {
      id: 'u4',
      name: 'Alex',
      imageUri: 'https://image.ibb.co/jOzeUG/luke_1.jpg',
    }],
    lastMessage: {
      id: 'm3',
      content: 'Hi, Vadim.',
      createdAt: '2020-10-02T14:48:00.000Z',
    }
  }, {
    id: '4',
    users: [{
      id: 'u1',
      name: 'Vadim',
      imageUri: 'https://image.ibb.co/k0wVTm/profile_pic.jpg',
    }, {
      id: 'u5',
      name: 'Vlad',
      imageUri: 'https://image.ibb.co/cBZPww/bane_1.jpg',
    }],
    lastMessage: {
      id: 'm4',
      content: 'Can you review my last merge',
      createdAt: '2020-09-29T14:48:00.000Z',
    }
  }, {
    id: '5',
    users: [{
      id: 'u1',
      name: 'Vadim',
      imageUri: 'https://image.ibb.co/k0wVTm/profile_pic.jpg',
    }, {
      id: 'u6',
      name: 'Elon Musk',
      imageUri: 'https://image.ibb.co/j4Ov3b/darth_vader_1.png',
    }],
    lastMessage: {
      id: 'm5',
      content: 'I would be happy',
      createdAt: '2020-09-30T14:48:00.000Z',
    }
  }, {
    id: '6',
    users: [{
      id: 'u1',
      name: 'Vadim',
      imageUri: 'https://image.ibb.co/k0wVTm/profile_pic.jpg',
    }, {
      id: 'u7',
      name: 'Adrian',
      imageUri: 'https://image.ibb.co/b4kxGw/zach_1.jpg',
    }],
    lastMessage: {
      id: 'm6',
      content: 'I have a solution',
      createdAt: '2020-10-02T15:40:00.000Z',
    }
  }, {
    id: '7',
    users: [{
      id: 'u1',
      name: 'Vadim',
      imageUri: 'https://image.ibb.co/k0wVTm/profile_pic.jpg',
    }, {
      id: 'u8',
      name: 'Borja',
      imageUri: 'https://image.ibb.co/fQKPww/kennith_1.jpg',
    }],
    lastMessage: {
      id: 'm7',
      content: 'How are you doing?',
      createdAt: '2020-10-02T15:40:00.000Z',
    }
  }, {
    id: '8',
    users: [{
      id: 'u1',
      name: 'Vadim',
      imageUri: 'https://image.ibb.co/k0wVTm/profile_pic.jpg',
    }, {
      id: 'u9',
      name: 'Mom',
      imageUri: 'https://image.ibb.co/ncAa3b/chloe_1.jpg',
    }],
    lastMessage: {
      id: 'm8',
      content: 'Dear, did you eat?',
      createdAt: '2020-09-27T15:40:00.000Z',
    }
  }, {
    id: '9',
    users: [{
      id: 'u1',
      name: 'Vadim',
      imageUri: 'https://image.ibb.co/k0wVTm/profile_pic.jpg',
    }, {
      id: 'u10',
      name: 'Angelina Jolie',
      imageUri: 'https://image.ibb.co/eLVWbw/katie_1.jpg',
    }],
    lastMessage: {
      id: 'm9',
      content: 'Meet me at the same place',
      createdAt: '2020-09-25T15:40:00.000Z',
    },
  }]